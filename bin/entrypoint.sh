#!/bin/bash
# TODO: verify env before envsubst
set -eu
set -o pipefail

: "${LDAP_LISTEN_PORT:=33389}"
: "${LDAP_LISTEN_URIS:="ldap://$(hostname):${LDAP_LISTEN_PORT} ldap://localhost:${LDAP_LISTEN_PORT} ldapi:///"}"
: "${LDAP_SYNC_INTERVAL:=00:00:30:00}"
export LDAP_SYNC_INTERVAL LDAP_LISTEN_PORT LDAP_LISTEN_URIS

log() {
    echo "[$(date -Is)] $*"
}

generate_config() {
    local ldif

    if [ -f "/etc/openldap/slapd.d/cn=config.ldif" ]; then
        log "Don't regenerate cn=config, it already exists."
        return
    fi

    GID="$(id -g)"
    export GID UID

    case "$1" in
        /*)
            ldif="$1"
            ;;
        *)
            if [ -f "$TMPLDIR/$1" ]; then
                ldif="$TMPLDIR/$1"
            elif [ -f "$TMPLDIR/$1.tmpl" ]; then
                ldif="$TMPLDIR/$1.tmpl"
            else
                log "Template $1 does not exist!"
                exit 1
            fi
            ;;
    esac
    rm -fr /etc/openldap/slapd.d
    mkdir -p /data/ldap/config /data/ldap/data /etc/openldap/slapd.d
    envsubst < "$ldif" | /usr/sbin/slapadd -b cn=config -F /etc/openldap/slapd.d
}

add_config_repl() {
    local n="$1"
    local hostname localpart
    hostname="$(hostname)"
    localpart="${hostname%-?}"

    export LDAP_PROVIDER_URI="ldap://${localpart}-${n}:${LDAP_LISTEN_PORT}"
    envsubst < "$TMPLDIR/add_syncrepl.tmpl"
}


run() {
    local args

    if [ $# -eq 0 ]; then
        args=(-h "$LDAP_LISTEN_URIS" -d config)
    else
        args=("$@")
    fi
    set -x
    exec /usr/sbin/slapd "${args[@]}"
}

if [ $# -ge 1 ]; then
    case "$1" in
        consumer)
            generate_config simple_consumer
            ;;
        multimaster)
            if [ $# != 2 ]; then
                log "Not specified how many multimaster-peers there should be"
                log "USAGE: multimaster N"
            fi

            IFS='^J' DB0_CNCONFIG_APPENDUM="$(for ((n=0; n < $2; n++)); do
                export n
                add_config_repl "$n"
            done)"

            export DB0_CNCONFIG_APPENDUM
            generate_config simple_multimaster
            shift
            ;;
        generate_config)
            if [ $# -ge 2 ]; then
                generate_config "$2"
                shift
            else
                log "no config given to generate"
                exit 1
            fi
            ;;
    esac
    shift
fi

run "$@"
